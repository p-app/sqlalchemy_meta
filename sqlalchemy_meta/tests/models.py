from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import declarative_base, scoped_session, sessionmaker

from sqlalchemy_meta import SessionDeclarativeMeta

DeclarativeBase: SessionDeclarativeMeta = declarative_base(
    name='DeclarativeBase',
    metaclass=SessionDeclarativeMeta,
)
DeclarativeBase.db_session = scoped_session(sessionmaker())


class SimpleModel(DeclarativeBase):  # type: ignore
    __tablename__ = 'simple_model'

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    desc = Column(String)


class ThreeKeyModel(DeclarativeBase):  # type: ignore
    __tablename__ = 'three_key_model'

    id1 = Column(Integer, primary_key=True, autoincrement=False)
    id2 = Column(Integer, primary_key=True, autoincrement=False)
    id3 = Column(Integer, primary_key=True)
    name = Column(String)


class NonIdModel(DeclarativeBase):  # type: ignore
    __tablename__ = 'non_id_model'

    id1 = Column(Integer, primary_key=True, autoincrement=False)
    name = Column(String)
    simple_model_id = Column(Integer, ForeignKey('simple_model.id'))


class HybridPropertyModel(DeclarativeBase):  # type: ignore
    __tablename__ = 'hybrid_property_model'

    id = Column(Integer, primary_key=True)
    name = Column(String)

    @hybrid_property
    def hybrid_name(self):
        return self.name


class SetableHybridPropertyModel(DeclarativeBase):  # type: ignore
    __tablename__ = 'setable_hybrid_property_model'

    id = Column(Integer, primary_key=True)
    name = Column(String)

    @hybrid_property
    def hybrid_name(self):
        return self.name

    @hybrid_name.setter  # type: ignore[no-redef]
    def hybrid_name(self, value):
        self.name = value


class PrimaryHybridPropertyModel(DeclarativeBase):  # type: ignore
    __tablename__ = 'primary_hybrid_property_model'

    id = Column(Integer, primary_key=True)
    name = Column(String)

    @hybrid_property
    def id1(self):
        return self.id

    @id1.setter  # type: ignore[no-redef]
    def id1(self, value):
        self.id = value  # pylint: disable=invalid-name
