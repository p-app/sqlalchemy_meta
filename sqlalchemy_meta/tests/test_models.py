# -*- coding: utf-8 -*-
from operator import attrgetter, itemgetter
from random import randint

import pytest
from sqlalchemy.exc import IntegrityError

from .models import HybridPropertyModel, NonIdModel, \
    PrimaryHybridPropertyModel, SetableHybridPropertyModel, SimpleModel, \
    ThreeKeyModel


@pytest.mark.usefixtures('sa_clear')
def test_simple_model():
    models = [
        SimpleModel.create(
            id=(6000 + i), name=f'test {i}', k=3,
        ) for i in range(5)
    ]
    assert models[0].id == 6000
    assert models[0].name == 'test 0'

    model_ids = list(map(attrgetter('id'), models))
    SimpleModel.commit()

    def get_all(*args, **kwargs):
        return list(map(attrgetter('id'), SimpleModel.all(*args, **kwargs)))

    assert get_all() == model_ids

    assert get_all(SimpleModel.id == model_ids[3]) == [model_ids[3]]

    assert get_all(SimpleModel.name.endswith('2')) == [model_ids[2]]

    assert get_all(order_by=SimpleModel.id.desc()) == list(reversed(model_ids))

    assert get_all(order_by=(SimpleModel.name.desc(), SimpleModel.id)) == \
        list(reversed(model_ids))

    assert get_all(order_by=SimpleModel.name.desc(), start=2, stop=4) == \
        list(reversed(model_ids))[2:4]

    assert get_all(order_by=SimpleModel.name.desc(), start=2) == \
        list(reversed(model_ids))[2:]

    assert get_all(order_by=SimpleModel.name.desc(), stop=4) == \
        list(reversed(model_ids))[:4]

    def get_byid(*args, **kwargs):
        return list(map(attrgetter('id'), SimpleModel.byid(*args, **kwargs)))

    assert SimpleModel.byid(model_ids[1], first=True).id == model_ids[1]
    assert get_byid(model_ids[1]) == [model_ids[1]]
    assert get_byid(model_ids[1], model_ids[2]) == [model_ids[1], model_ids[2]]
    assert get_byid(
        model_ids[1], model_ids[2], order_by=SimpleModel.id.desc(),
    ) == [model_ids[2], model_ids[1]]

    assert SimpleModel.first().id == model_ids[0]
    assert SimpleModel.first(start=2).id == model_ids[2]

    NonIdModel.create(name='test', simple_model_id=model_ids[0], id1=9)

    with pytest.raises(IntegrityError):
        SimpleModel.byid(model_ids[0], first=True).delete()

    SimpleModel.byid(model_ids[0], first=True).delete(silent=True)
    assert SimpleModel.byid(model_ids[0], first=True) is not None

    SimpleModel.byid(model_ids[2], first=True).delete()
    assert SimpleModel.byid(model_ids[2], first=True) is None

    model = SimpleModel.create(name='name', desc='desc', id=2000)
    model.edit(name='name 2', id=3000)
    assert model.name == 'name 2'
    assert model.desc == 'desc'
    assert model.id == 3000


@pytest.mark.usefixtures('sa_clear')
def test_three_key_model():
    models = [
        ThreeKeyModel.create(
            id1=randint(1000, 1010),
            id2=randint(1000, 1010),
            id3=randint(1000, 1010),
            name=f'test {i}',
            m=3,
        ) for i in range(5)
    ]
    model_ids = [
        dict(
            id1=m.id1,
            id2=m.id2,
            id3=m.id3,
        ) for m in models
    ]
    ThreeKeyModel.commit()

    manual_ids = set(range(1000, 1011))
    # All ids, including autoincrement, was set
    assert set(map(attrgetter('id3'), models)) & manual_ids != set()
    assert set(map(attrgetter('id2'), models)) & manual_ids != set()
    assert set(map(attrgetter('id1'), models)) & manual_ids != set()

    model = ThreeKeyModel.byid(model_ids[0], first=True)
    assert model.id1 == model_ids[0]['id1']
    assert model.id2 == model_ids[0]['id2']
    assert model.id3 == model_ids[0]['id3']

    models = ThreeKeyModel.byid(
        model_ids[0], model_ids[2],
    )

    def assert_ids(id_key, ids):
        assert frozenset(
            map(
                attrgetter(id_key), models,
            ),
        ) == frozenset(
            map(
                itemgetter(id_key), ids,
            ),
        )

    assert_ids('id1', [model_ids[0], model_ids[2]])
    assert_ids('id2', [model_ids[0], model_ids[2]])
    assert_ids('id3', [model_ids[0], model_ids[2]])

    ThreeKeyModel.byid(model_ids[2], first=True).edit(id1=2001, id2=2003)
    ThreeKeyModel.commit()

    assert ThreeKeyModel.byid(model_ids[2], first=True) is None
    assert ThreeKeyModel.byid(
        {
            'id1': 2001, 'id2': 2003, 'id3': model_ids[2]['id3'],
        }, first=True,
    ).name == 'test 2'


@pytest.mark.usefixtures('sa_clear')
def test_non_id_model():
    models = [
        NonIdModel.create(id1=i, name=f'test {i}')
        for i in range(5)
    ]
    model_ids = list(map(attrgetter('id1'), models))

    assert list(map(attrgetter('id1'), NonIdModel.all())) == model_ids
    assert NonIdModel.byid(model_ids[2], first=True).id1 == model_ids[2]
    assert list(
        map(
            attrgetter('id1'), NonIdModel.byid(model_ids[2], model_ids[4]),
        ),
    ) == [model_ids[2], model_ids[4]]

    NonIdModel.byid(model_ids[2], first=True).edit(id1=10)
    NonIdModel.commit()

    assert NonIdModel.byid(model_ids[2], first=True) is None
    assert NonIdModel.byid(10, first=True) is not None

    NonIdModel.byid(10, first=True).delete()
    assert NonIdModel.byid(10, first=True) is None


@pytest.mark.usefixtures('sa_clear')
def test_hybrid_property_model():
    model = HybridPropertyModel.create(hybrid_name='test')
    assert model.name is None  # hybrid property have no setter

    model.edit(name='test')
    assert model.name == 'test'


@pytest.mark.usefixtures('sa_clear')
def test_setable_hybrid_property_model():
    model = SetableHybridPropertyModel.create(hybrid_name='test')
    assert model.name == 'test'  # value of hybrid key

    model = SetableHybridPropertyModel.create(
        hybrid_name='test',
        name='test 2',
    )
    assert model.name == 'test 2'  # value of original key

    model.edit(hybrid_name='test 3')
    assert model.name == 'test 3'

    model.edit(hybrid_name='test 3', name='test 4')
    assert model.name == 'test 4'

    model = SetableHybridPropertyModel.create(name='test')
    assert model.name == 'test'


@pytest.mark.usefixtures('sa_clear')
def test_primary_hybrid_property_model():
    model = PrimaryHybridPropertyModel.create(id=2000, id1=3000)
    assert model.id == 2000  # original key selected

    model = PrimaryHybridPropertyModel.create(id1=3000)
    assert model.id == 3000  # hybrid key selected
