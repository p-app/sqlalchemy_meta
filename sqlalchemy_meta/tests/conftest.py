from configparser import ConfigParser
from functools import partial
from operator import attrgetter

import pytest
from sqlalchemy import engine_from_config, text

from .models import DeclarativeBase


# pylint: disable=redefined-outer-name


def pytest_addoption(parser):
    parser.addoption(
        "--ini", action="store", metavar="INI_FILE",
        help="use INI_FILE to configure app",
    )


@pytest.fixture(scope='session')
def appsettings(request):
    parser = ConfigParser()
    parser.read(request.config.option.ini)
    return dict(parser['sqlalchemy_meta'].items())


# noinspection PyShadowingNames
@pytest.fixture(scope='session')
def sa_engine(appsettings, request):
    engine = engine_from_config(appsettings)
    metadata = DeclarativeBase.metadata
    metadata.bind = engine
    metadata.create_all(checkfirst=True)
    request.addfinalizer(partial(metadata.drop_all, checkfirst=True))
    return engine


# noinspection PyShadowingNames
@pytest.fixture(scope='function')
def sa_clear(sa_engine, request):
    db_session = DeclarativeBase.db_session
    request.addfinalizer(db_session.commit)
    request.addfinalizer(db_session.close)

    with sa_engine.begin() as conn:
        conn.execute(
            text(
                # pylint: disable=consider-using-f-string
                'TRUNCATE "{}" CASCADE'.format(
                    '", "'.join(
                        map(
                            attrgetter('name'),
                            reversed(DeclarativeBase.metadata.sorted_tables),
                        ),
                    ),
                ),
            ).execution_options(autocommit=True),
        )
