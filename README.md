## Sqlalchemy meta

This library provides creating [SQLAlchemy](https://www.sqlalchemy.org/) 
declarative object model with various meta classes, that bring additional 
functional.

Already implemented classes:
 - SessionDeclarativeMeta - meta class with scoped session additional methods.
